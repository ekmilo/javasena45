/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sena.moviles;

/**
 *
 * @author ekmil
 */
public class Persona extends Mamifero{ //superclase y clase hija de Mamifero
    
    public  int getEdad(){
        return 10;
    }
    
    @Override
    public void correr(String distancia){
            System.out.println("Persona está corriendo mucho " + distancia);
    }

    
    public void pensar(String pensamiento){
        System.out.println("pensando" + pensamiento);
    
    }
    protected String llorar(boolean dejonovia){
        if(dejonovia){
            return "si está llorando";
        }else{
            return "no está llorando";
        }
    }
    
}