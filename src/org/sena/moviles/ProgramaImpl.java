/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sena.moviles;

import java.util.List;

/**
 *
 * @author ekmil
 */
public class ProgramaImpl  implements Programa {
//Implementación de la interfaz programa, como hereda de otras interfaces
    //también se implementan los métodos de las otras interfaces
    public static int estatico=0; // variable estática es de la clase => ProgramaImpl.estatico
    public int noestatico=0; // variable del objeto, solo se accede desde un objeto
    
    //Implementación de métodos
    public void hacerPrograma() {
    }

    public void hacerFor(int veces) {
    }

    public void hacerWhile(List lista) {
     }

    public void hacerIf(Integer edad) {
    }

    public void hacerSwitch(Integer opcion) {
     }

    public void hacerLogica() {
    }
    
}
