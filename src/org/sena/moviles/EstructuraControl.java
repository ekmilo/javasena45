/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sena.moviles;

import java.util.List;

/**
 *
 * @author ekmil
 */
public interface EstructuraControl { // definición de interfaz
    
    public void hacerFor(int veces); // métodos abstractos de la interfaz
    public void hacerWhile(List lista);
    public void hacerIf(Integer edad);
    public void hacerSwitch(Integer opcion);
    
    
}
