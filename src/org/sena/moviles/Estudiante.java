/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sena.moviles;

/**
 *
 * @author ekmil
 */
public class Estudiante extends Persona{ //clase hija de Persona
      
   public Mamifero obtenerAmigo(Mamifero a){ // polimorfismo en métodos
    
       a.correr("3 km");
       
       return a;
   }
    
    
    public void correr(String distancia){
       System.out.println("Estudiante está corriendo mucho " + distancia);
      }
    
    public void estudiar(String nivelAcademico){
        System.out.println("esta estudiando " + nivelAcademico  );
                obtenerAmigo(new Estudiante()); // invocar métodos con polimorfismo
                obtenerAmigo(new Persona()); // invocar métodos con polimorfismo
    }
    
    public static void main(String []args){
        Mamifero m = new Mamifero();
        Persona p  = new Persona();
        Estudiante e = new Estudiante();
        
    //   m.correr("2 km");
     //   p.correr("2 km");
    //    e.correr("2 km");
        
        Mamifero mamifero = e;
     //   mamifero.correr("2 km ");
        e.estudiar("aa");
      
        
        Estudiante estudiante = (Estudiante)mamifero; // castear
        estudiante.correr("3km");
        
        SerVivo serVivo=new Mamifero();
        serVivo = new Persona();
        serVivo = new Estudiante();
        
      
        
    }
    
}
