/*
Comentario multilinea
*/
// comentario de una línea

package org.sena.moviles;

public class Calculadora{  // nombre de clase   public class Nombre{}
    
    private String marca; // Tipo de dato(String)     Nombre Variable (marca)  ;
    private Integer tamanio;   // se inicializa siendo nula
    private int algo; //float, double, boolean, char  - se inicializan con algun valor
    private String color="rojo"; // Público se puede llamar desde cualquier punto
    private Integer numeroBotones; // Privado solo se puede llamar en la misma clase
    private Integer numeroFunciones;

    public Calculadora(){  // Constructor tipo de acceso   Nombre de la clase   Parámetros
            this("marca",30); //llamo otro constructor
            marca = "casio";  //podemos iniciar atributos del objeto o hacer algo de lógica
            this.tamanio = 10;
            this.sumar(2, 4);
            System.out.println("se instancio un constructor");
            
    }
   
    private Calculadora(String marca, Integer tamanio){ // Constructor con parámetros
            this.marca = marca;
            this.tamanio = tamanio;   
    }
    
    public Calculadora(Calculadora c){  // Constructor copia, asigna los mismos atributos que el constructor
        //del parámetro
        this.algo = c.algo;
        this.marca = c.marca;
        this.tamanio = c. tamanio;
    } 
    
    public String obtenerColor(){
           return color;
    }
       
    public void cambiarColor(String nuevoColor){
            color=nuevoColor;
    }
    
    public Integer sumar(Integer num1, Integer num2){ // parámetros se separan por ,
           // tamanio=10;
           // restar();
           Integer resultado = num1+num2;
            System.out.println("estoy sumando " + resultado);
            return resultado; // el retorno debe ser igual al definido en el método en este caso Integer
    }
         
    private void restar(){  // método tipo de acceso   retorno   nombre y parámetros
        System.out.println("estoy restando");
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getTamanio() {
        return tamanio;
    }

    public void setTamanio(Integer tamanio) {
        this.tamanio = tamanio;
    }
//Getters y Setters:
    public int getAlgo() {
        return algo;
    }

    public void setAlgo(int algo) {
        this.algo = algo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getNumeroBotones() {
        return numeroBotones;
    }

    public void setNumeroBotones(Integer numeroBotones) {
        this.numeroBotones = numeroBotones;
    }

    public Integer getNumeroFunciones() {
        return numeroFunciones;
    }

    public void setNumeroFunciones(Integer numeroFunciones) {
        this.numeroFunciones = numeroFunciones;
    }
    
    public static void main(String [] args){
       /* Calculadora kmilo =new Calculadora() ; // creación de objeto Calculadora
        kmilo.sumar(2,5); // métodos del objeto, envío parámetros
        kmilo.sumar(12,3);
        kmilo.restar();
        kmilo.color="azul"; //atributos del objeto
        System.out.println("color " + kmilo.color.toLowerCase());
            */   
         Calculadora c = new Calculadora(); // Llamo uno de los constructores
         System.out.println("marca " + c.marca);
    
         Calculadora cdosparametros = new Calculadora("nokia", 30); // llamo constructor con 2 parámetros
          System.out.println("marca " + cdosparametros.marca);
          
        Calculadora copia = new Calculadora ("copia",12);
        Calculadora ca = new Calculadora ( copia );  // creo objeto usando el otro objeto
           System.out.println("marca " + ca.marca);
    }
            
    
}
