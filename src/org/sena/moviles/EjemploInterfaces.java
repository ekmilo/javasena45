/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sena.moviles;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ekmil
 */
public class EjemploInterfaces {
    
      public static void main(String [] args){
           
          EstructuraControl a = new EstructuraControlImpl(); 
            //Polimorfismo, la variable a se asigna el objeto EstructuraControlImpl
          //los métodos de a serán los del objeto EstructuraControlImpl
          
              EstructuraControl e = new ProgramaImpl();
           //Polimorfismo, la variable r se asigna el objeto ProgramaImpl
          //los métodos de e serán los del objeto ProgramaImpl
              
              Programa pro =(Programa) e; //Casteo para asegurar que e es un Programa
             
             System.out.println("hago for");
            a.hacerFor(2); // el método del objeto EstructuraControlImpl
             System.out.println("hago if");
            a.hacerIf(20);
            List lista = new ArrayList(); lista.add(3);lista.add(2);lista.add(4);
             System.out.println("hago while");
            a.hacerWhile(lista);
             System.out.println("hago switch");
            a.hacerSwitch(1);
             System.out.println("hago for");
            e.hacerFor(3);// el método del objeto ProgramaImpl
            
          
            
            
    }
    
}
