/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sena.moviles;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ekmil
 */
public class EstructuraControlImpl  implements EstructuraControl{ 
// implementación de interfaz EstructuraControl
    
     public void hacerFor(int veces){ // método implementado
         List<Integer> lista= new ArrayList<Integer>(); // definición de lista de Integer
         for(int i =0; i <veces;i++){ // for (inicializacion de variable ; condicion booleana; incremento)
             lista.add(i);
         }
         for(int i=0;i<lista.size();i++){ // lista.size() tamaño de la lista
             System.out.println(lista.get(i)); // obtener elemento en la posicion i de la lista
         }
     }
    public void hacerWhile(List lista){ // recibo parámetro lista
        int i=0;
        while(i<lista.size()){ // mientras que i < lista.size() haga lo que está adentro
            System.out.println(lista.get(i)); // obtengo elemento de la lista en la posicion i
            i++; //incremento
        }
    }
    public void hacerIf(Integer edad){
        if(edad >= 0 && edad <18){  // si (condición) haga 
            System.out.println("menor");
        }
        else if(edad >18 && edad <50){ // sino si (condición)
            System.out.println("adulto");
        }
        else{ // sino haga..
            System.out.println("viejo");
        }
  
    }
    public void hacerSwitch(Integer opcion){
           switch(opcion){ // según el valor que tome la opción
               case 1: // si opción == 1
                   System.out.println("1"); // imprima 1 
               case 2: // aquí no hay break por lo que sigue con la opción 2
                   System.out.println("2"); // imprime 2
                   break; // el break interrumpe el bloque y nos saca del switch
               default: // opción por defecto se llama si no se cumplen las demás
                   System.out.println("opcion por defecto si no se cumple las otras");
               break;   
           }
    
    }
    
}
